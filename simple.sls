;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-FileCopyrightText: 2010-2022 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Custom input and output ports that start a TLS connection

;; TODO: avoid all this extra copying

(library (terrible-tls simple)
  (export
    tls-connect
    start-tls)
  (import
    (rnrs (6))
    (only (srfi :1 lists) last)
    (industria bytevectors)
    (industria crypto rsa)
    (industria tcp)
    (terrible-tls client)
    (terrible-tls x509))

(define-syntax print
  (syntax-rules ()
    #;
    ((_ . args)
     (begin
       (for-each display (list . args))
       (newline)))
    ((_ . args) (values))))

(define-syntax trace
  (syntax-rules ()
    #;
    ((_ x)
     (begin
       (let ((v x))
         (display "TLS: ")
         (display v)
         (newline)
         v)))
    ((_ x) x)))

;; TODO: let client.sls handle fragmentation
(define maxlen (expt 2 14))

(define tls-connect
  (case-lambda
    ((host service)
     (tls-connect host service '() #f))
    ((host service client-certificates client-private-key)
     (let-values (((in out) (tcp-connect host service)))
       (start-tls host service in out client-certificates client-private-key)))))

(define start-tls
  (case-lambda
    ((host service in out)
     (start-tls host service in out '() #f))
    ((host service in out client-certificates client-private-key)
     (when client-private-key
       ;; Verify that the certificate matches the private key.
       (let ((cert-key (certificate-public-key (last client-certificates)))
             (key (rsa-private->public client-private-key)))
         (unless (and (= (rsa-public-key-modulus cert-key)
                         (rsa-public-key-modulus key))
                      (= (rsa-public-key-public-exponent cert-key)
                         (rsa-public-key-public-exponent key)))
           (error 'start-tls
                  "The last of the client certificates does not match the key"
                  cert-key key))))

     (let ((s (make-tls-wrapper in out host))
           (send-cert? #f)
           (other-closed? #f)
           (unread #f)
           (offset 0))
       (define (fail condition^)
         (close-output-port out)
         (close-input-port in)
         (raise
           (condition
            (make-error)
            (make-i/o-error)
            (make-i/o-port-error in)
            condition^)))

       (define (read! bytevector start count)
         ;; Read up to `count' bytes from the source, write them to
         ;; `bytevector' at index `start'. Return the number of bytes
         ;; read (zero means end of file).
         (define (return data offset*)
           (let* ((valid (- (bytevector-length data) offset*))
                  (returned (min count valid)))
             (cond ((= returned valid)
                    (set! unread #f)
                    (set! offset 0))
                   (else
                    (set! unread data)
                    (set! offset (+ offset returned))))
             (bytevector-copy! data offset* bytevector start returned)
             returned))
         (if unread
             (return unread offset)
             (let lp ()
               (let ((r (get-tls-record s)))
                 (cond ((and (pair? r) (eq? (car r) 'application-data))
                        (if (zero? (bytevector-length (cadr r)))
                            (lp)
                            (return (cadr r) 0)))
                       ((condition? r)
                        ;; XXX: other alerts can also cause it to
                        ;; close... but in error.
                        (if (equal? (condition-irritants r) '((0 . close-notify)))
                            0
                            (lp)))
                       ((eof-object? r)
                        0)
                       ;; FIXME: do something about this record...
                       (else (lp)))))))

       (define (write! bytevector start count)
         ;; Send up to `count' bytes from `bytevector' at index
         ;; `start'. Returns the number of bytes written. A zero count
         ;; should send a close-notify.
         (cond ((zero? count)
                (put-tls-alert-record s 1 0)
                (flush-tls-output s)
                0)
               (else
                (do ((rem count (- rem maxlen))
                     (idx start (+ idx maxlen)))
                    ((<= rem 0)
                     (flush-tls-output s)
                     count)
                  (put-tls-application-data s (subbytevector bytevector idx
                                                             (+ idx (min maxlen rem))))))))

       (define (close)
         (cond (other-closed?
                (put-tls-alert-record s 1 0)
                (guard (con
                        ((i/o-error? con) #f))
                  (flush-tls-output s)
                  (close-output-port out))
                (close-input-port in))
               (else
                ;; This is so that each port can be closed independently.
                (set! other-closed? #t))))
       (define (expect want)
         (let ((got (trace (get-tls-record s))))
           (unless (eq? got want)
             (if (condition? got)
                 (fail got)
                 (fail (condition
                        (make-message-condition
                         (string-append "Expected " (symbol->string want)))
                        (make-irritants-condition (list got))))))))

       ;; Start negotiation
       (put-tls-handshake-client-hello s)
       (flush-tls-output s)

       (expect 'handshake-server-hello)

       (let lp ((allowed '(handshake-certificate
                           handshake-server-key-exchange
                           handshake-certificate-request
                           handshake-server-hello-done)))
         (let ((data (trace (get-tls-record s))))
           (when (eof-object? data)
             (fail (make-message-condition "The server disconnected during the handshake")))
           (unless (memq data allowed)
             (fail (condition
                    (make-message-condition
                     "The server did the handshake in the wrong order")
                    (make-irritants-condition (list data)))))
           (case data
             ((handshake-certificate-request)
              (set! send-cert? #t)
              (lp '(handshake-server-hello-done)))
             ((handshake-server-hello-done) #t)
             (else
              (lp (cdr (memq data allowed)))))))

       (print "Server handshake done. The client sends its own handshake now.")
       (when send-cert?
         (print "Sending " (length client-certificates) " client certificates.")
         (put-tls-handshake-certificate s client-certificates))
       (put-tls-handshake-client-key-exchange s)
       (when (and send-cert? (not (null? client-certificates)))
         (put-tls-handshake-certificate-verify s client-private-key))
       (put-tls-change-cipher-spec s)
       (put-tls-handshake-finished s)
       (flush-tls-output s)

       (expect 'change-cipher-spec)
       (expect 'handshake-finished)

       (let ((id (string-append "tls " host ":" service)))
         (values (make-custom-binary-input-port id read! #f #f close)
                 (make-custom-binary-output-port id write! #f #f close)
                 s)))))))
