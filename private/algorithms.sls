;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-FileCopyrightText: 2009-2022 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Cryptographical algorithms for the TLS library

(library (terrible-tls private algorithms)
  (export
    tls-hash
    tls-hash-algorithm->oid
    tls-extension-supported-signature-algorithms
    tls-extension-supported-groups
    TLS-SIGNATURE-ANONYMOUS
    TLS-SIGNATURE-RSA
    TLS-SIGNATURE-DSA
    TLS-SIGNATURE-ECDSA
    TLS-HASH-NONE
    TLS-HASH-MD5
    TLS-HASH-SHA-1
    TLS-HASH-SHA-224
    TLS-HASH-SHA-256
    TLS-HASH-SHA-384
    TLS-HASH-SHA-512
    TLS-HASH-EdDSA

    TLS-GROUP-SECP256R1
    TLS-GROUP-SECP384R1
    TLS-GROUP-SECP521R1
    TLS-GROUP-X25519
    TLS-GROUP-X448

    tls-prf-md5-sha1
    tls-prf-sha256

    null-cipher-suite supported-cipher-suites

    make-cs
    cs-name cs-id cs-kex cs-cipher cs-mac
    cs-key-length cs-fixed-iv-size cs-record-iv-size cs-block-size
    cs-expand-ekey cs-expand-dkey
    cs-hash-size #;cs-mac-key-length
    cs-verify-data-length

    cs-compute-mac cs-encrypt! cs-decrypt!)
  (import
    (rnrs (6))
    (srfi :39 parameters)
    (only (industria bytevectors) bytevector-concatenate subbytevector)
    (struct pack)
    (industria crypto aes)
    ;;(industria crypto dh)
    (industria crypto dsa)
    ;;(industria crypto entropy)
    (industria crypto rsa)
    (hashing md5)
    (hashing sha-1)
    (hashing sha-2))

;;; Signature and hash algorithms

(define TLS-SIGNATURE-ANONYMOUS  0)
(define TLS-SIGNATURE-RSA        1)
(define TLS-SIGNATURE-DSA        2)
(define TLS-SIGNATURE-ECDSA      3)
(define TLS-SIGNATURE-ED25519    7)     ;RFC8422
(define TLS-SIGNATURE-ED448      8)     ;RFC8422

(define TLS-HASH-NONE    0)
(define TLS-HASH-MD5     1)
(define TLS-HASH-SHA-1   2)
(define TLS-HASH-SHA-224 3)
(define TLS-HASH-SHA-256 4)
(define TLS-HASH-SHA-384 5)
(define TLS-HASH-SHA-512 6)
(define TLS-HASH-EdDSA   8)             ;RFC8422

(define (tls-hash hash-algo bv*)
  (cond ((eqv? hash-algo TLS-HASH-MD5) (md5->bytevector (apply md5 bv*)))
        ((eqv? hash-algo TLS-HASH-SHA-1) (sha-1->bytevector (apply sha-1 bv*)))
        ((eqv? hash-algo TLS-HASH-SHA-224) (sha-224->bytevector (apply sha-224 bv*)))
        ((eqv? hash-algo TLS-HASH-SHA-256) (sha-256->bytevector (apply sha-256 bv*)))
        ((eqv? hash-algo TLS-HASH-SHA-384) (sha-384->bytevector (apply sha-384 bv*)))
        ((eqv? hash-algo TLS-HASH-SHA-512) (sha-512->bytevector (apply sha-512 bv*)))
        (else
         (error 'tls-hash "Unsupported hash algorithm" hash-algo))))

(define (tls-hash-algorithm->oid hash-algo)
  (define id-md5    '(1 2 840 113549 2 5))
  (define id-sha-1  '(1 3 14 3 2 26))
  (define id-sha256 '(2 16 840 1 101 3 4 2 1))
  (define id-sha384 '(2 16 840 1 101 3 4 2 2))
  (define id-sha512 '(2 16 840 1 101 3 4 2 3))
  (define id-sha224 '(2 16 840 1 101 3 4 2 4))
  (cond ((eqv? hash-algo TLS-HASH-MD5)     id-md5)
        ((eqv? hash-algo TLS-HASH-SHA-1)   id-sha-1)
        ((eqv? hash-algo TLS-HASH-SHA-224) id-sha224)
        ((eqv? hash-algo TLS-HASH-SHA-256) id-sha256)
        ((eqv? hash-algo TLS-HASH-SHA-384) id-sha384)
        ((eqv? hash-algo TLS-HASH-SHA-512) id-sha512)
        (else
         (error 'tls-hash-algorithm->oid
                "Unsupported hash algorithm" hash-algo))))

(define (tls-extension-supported-signature-algorithms)
  (define (extval-signature-algorithm hash signature)
    (pack "CC" hash signature))
  (list
   (extval-signature-algorithm TLS-HASH-SHA-512 TLS-SIGNATURE-RSA)
   (extval-signature-algorithm TLS-HASH-SHA-384 TLS-SIGNATURE-RSA)
   (extval-signature-algorithm TLS-HASH-SHA-256 TLS-SIGNATURE-RSA)
   (extval-signature-algorithm TLS-HASH-SHA-224 TLS-SIGNATURE-RSA)
   (extval-signature-algorithm TLS-HASH-SHA-1 TLS-SIGNATURE-RSA)
   (extval-signature-algorithm TLS-HASH-MD5 TLS-SIGNATURE-RSA)

   ;; (extval-signature-algorithm TLS-HASH-EdDSA TLS-SIGNATURE-ED25519)
   ;; (extval-signature-algorithm TLS-HASH-EdDSA TLS-SIGNATURE-ED448)

   (extval-signature-algorithm TLS-HASH-SHA-512 TLS-SIGNATURE-ECDSA)
   (extval-signature-algorithm TLS-HASH-SHA-384 TLS-SIGNATURE-ECDSA)
   (extval-signature-algorithm TLS-HASH-SHA-256 TLS-SIGNATURE-ECDSA)
   (extval-signature-algorithm TLS-HASH-SHA-224 TLS-SIGNATURE-ECDSA)
   ))


(define TLS-GROUP-SECP256R1 23)
(define TLS-GROUP-SECP384R1 24)
(define TLS-GROUP-SECP521R1 25)
(define TLS-GROUP-X25519    29)
(define TLS-GROUP-X448      30)

(define (tls-extension-supported-groups)
  (define (group n)
    (pack "!S" n))
  (list
   (group TLS-GROUP-SECP256R1)
   (group TLS-GROUP-SECP384R1)
   (group TLS-GROUP-SECP521R1)
   ;; (group TLS-GROUP-X25519)
   ;; (group TLS-GROUP-X448)
   ))


;;; Pseudo random functions

(define (p-hash hash->bytevector hmac hash-length length secret seeds)
  ;; Expands the secret and seeds to at least `length' bytes, which
  ;; will be rounded to a multiple of the hash-length.
  (define (gen-A i max prev)
    (if (fx=? i max)
        '()
        (let ((a (hash->bytevector (hmac secret prev))))
          (cons prev (gen-A (fx+ i 1) max a)))))
  (bytevector-concatenate
   (map (lambda (a) (hash->bytevector (apply hmac secret a seeds)))
        (gen-A 0
               (div (+ length (- hash-length 1)) hash-length)
               (hash->bytevector (apply hmac secret seeds))))))

(define (tls-prf-sha256 bytes secret label seeds)
  ;; Performs this operation from RFC5246:
  ;; PRF(secret, label, seed) = P_<hash>(secret, label + seed)
  (let ((seeds (cons label seeds)))
    (let ((p (p-hash sha-256->bytevector hmac-sha-256 16 bytes secret seeds)))
      (subbytevector p 0 bytes))))

(define (tls-prf-md5-sha1 bytes secret label seeds)
  ;; Performs this operation from RFC4346:
  ;; PRF(secret, label, seed) = P_MD5(S1, label + seed) XOR
  ;;                          P_SHA-1(S2, label + seed)
  (let* ((half-length (div (+ (bytevector-length secret) 1) 2))
         (s1 (subbytevector secret 0 half-length))
         (s2 (subbytevector secret half-length))
         (seeds (cons label seeds)))
    (do ((p1 (p-hash md5->bytevector hmac-md5 16 bytes s1 seeds))
         (p2 (p-hash sha-1->bytevector hmac-sha-1 20 bytes s2 seeds))
         (i 0 (fx+ i 1))
         (ret (make-bytevector bytes)))
        ((= i bytes) ret)
      (bytevector-u8-set! ret i (fxxor (bytevector-u8-ref p1 i)
                                       (bytevector-u8-ref p2 i))))))

;;; Cipher suites

;; TODO: In TLS 1.2 the ciphersuite can specify a different PRF, and a
;; hash function for the Finished computation.
(define-record-type cs
  (fields name                          ;algorithm name
          id                            ;algorithm identifier (u16)
          kex                           ;key exchange algorithm
          cipher                        ;cipher algorithm
          mac                           ;MAC algorithm
          key-length                    ;cipher key length
          fixed-iv-size                 ;fixed client/server IV size
          record-iv-size                ;per-record IV size
          block-size                    ;cipher block size
          expand-ekey                   ;expand encryption key schedule
          expand-dkey                   ;expand decryption key schedule
          hash-size                     ;MAC hash size
          mac-key-length                ;MAC key length
          verify-data-length)           ;verify_data length
  (protocol
   (lambda (p)
     (lambda (name id kex cipher mac prf)
       (let-values
           (((key-length fixed-iv-size record-iv-size block-size expand-ekey expand-dkey)
             ;; Cipher parameters
             (case cipher
               ((#f)
                (values 0 0 0 #f 'no-keys 'no-keys))
               ((aes-128-cbc)
                (values 16 16 16 16
                        expand-aes-key
                        (lambda (k)
                          (reverse-aes-schedule
                           (expand-aes-key k)))))
               ((aes-256-cbc)
                (values 32 16 16 16
                        expand-aes-key
                        (lambda (k)
                          (reverse-aes-schedule
                           (expand-aes-key k)))))
               ((aes-128-gcm)
                ;; FIXME: note that the ciphertext becomes 16 bytes
                ;; larger thanks to the tag.
                (letrec ((expand-key/gcm
                          (lambda (k)
                            (make-aes-gcm-state (expand-aes-key k)))))
                  (values 16 4 8 #f expand-key/gcm expand-key/gcm)))
               (else
                (error 'make-cs "Unknown cipher algorithm" cipher))))
            ((hash-size mac-key-length)
             ;; MAC parameters
             (case mac
               ((#f) (values 0 0))
               ((md5) (values 16 16))
               ((sha-1) (values 20 20))
               ((sha-224) (values 28 28))
               ((sha-256) (values 32 32)) ;block size here and above is 64
               ((sha-384) (values 48 48)) ;block size is 128
               ((sha-512) (values 64 64)) ;block size is 128
               (else
                (error 'make-cs "Unknown MAC algorithm" mac)))))
         (p name id kex cipher mac
            key-length fixed-iv-size record-iv-size block-size expand-ekey expand-dkey
            hash-size mac-key-length 12))))))

(define null-cipher-suite (make-cs 'NULL-WITH-NULL-NULL #x0000 #f #f #f #f))

;; All the supported cipher suites in order of priority.
;; http://www.iana.org/assignments/tls-parameters/tls-parameters.txt
(define %supported-cipher-suites
  (list
   ;; RFC 5289

   ;;   CipherSuite TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256  = {0xC0,0x23};
   ;;   CipherSuite TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384  = {0xC0,0x24};
   ;;   CipherSuite TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256   = {0xC0,0x25};
   ;;   CipherSuite TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384   = {0xC0,0x26};
   ;;   CipherSuite TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256    = {0xC0,0x27};
   ;;   CipherSuite TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384    = {0xC0,0x28};
   ;;   CipherSuite TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256     = {0xC0,0x29};
   ;;   CipherSuite TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384     = {0xC0,0x2A};

   ;; o  For cipher suites ending with _SHA256, the PRF is the TLS PRF
   ;;    [RFC5246] with SHA-256 as the hash function.  The MAC is HMAC
   ;;    [RFC2104] with SHA-256 as the hash function.

   ;; o  For cipher suites ending with _SHA384, the PRF is the TLS PRF
   ;;    [RFC5246] with SHA-384 as the hash function.  The MAC is HMAC
   ;;    [RFC2104] with SHA-384 as the hash function.

   ;;   CipherSuite TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256  = {0xC0,0x2B};
   (make-cs 'ECDHE-ECDSA-WITH-AES-128-GCM-SHA384 #xC02B 'ecdhe-ecdsa 'aes-128-gcm #f 'sha-256)
   ;;   CipherSuite TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384  = {0xC0,0x2C};
   ;; (make-cs 'ECDHE-ECDSA-WITH-AES-256-GCM-SHA384 #xC02C 'ecdhe-ecdsa 'aes-256-gcm #f 'sha-384)

   ;;   CipherSuite TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256   = {0xC0,0x2D};
   ;;   CipherSuite TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384   = {0xC0,0x2E};
   ;;   CipherSuite TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256    = {0xC0,0x2F};
   ;;   CipherSuite TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384    = {0xC0,0x30};
   ;;   CipherSuite TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256     = {0xC0,0x31};
   ;;   CipherSuite TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384     = {0xC0,0x32};

   ;; (make-cs 'DH-DSS-WITH-AES-128-CBC-SHA256 #x003E 'dh-dss 'aes-128-cbc 'sha-256 #f)
   ;; (make-cs 'DH-RSA-WITH-AES-128-CBC-SHA256 #x003F 'dh-rsa 'aes-128-cbc 'sha-256 #f)
   ;; (make-cs 'DHE-DSS-WITH-AES-128-CBC-SHA256 #x0040 'dhe-dss 'aes-128-cbc 'sha-256 #f)
   (make-cs 'DHE-RSA-WITH-AES-128-CBC-SHA256 #x0067 'dhe-rsa 'aes-128-cbc 'sha-256 #f)
   ;; (make-cs 'DH-DSS-WITH-AES-256-CBC-SHA256 #x0068 'dh-dss 'aes-256-cbc 'sha-256 #f)
   ;; (make-cs 'DH-RSA-WITH-AES-256-CBC-SHA256 #x0069 'dh-rsa 'aes-256-cbc 'sha-256 #f)
   ;; (make-cs 'DHE-DSS-WITH-AES-256-CBC-SHA256 #x006A 'dhe-dss 'aes-256-cbc 'sha-256 #f)
   (make-cs 'DHE-RSA-WITH-AES-256-CBC-SHA256 #x006B 'dhe-rsa 'aes-256-cbc 'sha-256 #f)
   ;; (make-cs 'DH-DSS-WITH-AES-128-CBC-SHA #x0030 'dh-dss 'aes-128-cbc 'sha-1 #f)
   ;; (make-cs 'DH-RSA-WITH-AES-128-CBC-SHA #x0031 'dh-rsa 'aes-128-cbc 'sha-1 #f)
   (make-cs 'DHE-DSS-WITH-AES-128-CBC-SHA #x0032 'dhe-dss 'aes-128-cbc 'sha-1 #f)
   (make-cs 'DHE-RSA-WITH-AES-128-CBC-SHA #x0033 'dhe-rsa 'aes-128-cbc 'sha-1 #f)
   ;; (make-cs 'DH-DSS-WITH-AES-256-CBC-SHA #x0036 'dh-dss 'aes-256-cbc 'sha-1 #f)
   ;; (make-cs 'DH-RSA-WITH-AES-256-CBC-SHA #x0037 'dh-rsa 'aes-256-cbc 'sha-1 #f)
   (make-cs 'DHE-DSS-WITH-AES-256-CBC-SHA #x0038 'dhe-dss 'aes-256-cbc 'sha-1 #f)
   (make-cs 'DHE-RSA-WITH-AES-256-CBC-SHA #x0039 'dhe-rsa 'aes-256-cbc 'sha-1 #f)
   (make-cs 'RSA-WITH-AES-128-CBC-SHA256 #x003C 'rsa 'aes-128-cbc 'sha-256 #f)
   (make-cs 'RSA-WITH-AES-256-CBC-SHA256 #x003D 'rsa 'aes-256-cbc 'sha-256 #f)
   (make-cs 'RSA-WITH-AES-128-CBC-SHA #x002F 'rsa 'aes-128-cbc 'sha-1 #f) ;mandatory, TLS1.2
   (make-cs 'RSA-WITH-AES-256-CBC-SHA #x0035 'rsa 'aes-256-cbc 'sha-1 #f)
   ))

(define supported-cipher-suites (make-parameter %supported-cipher-suites))

(define (cs-compute-mac cs secret header data)
  (case (cs-mac cs)
    ((sha-1) (sha-1->bytevector (hmac-sha-1 secret header data)))
    ((sha-256) (sha-256->bytevector (hmac-sha-256 secret header data)))
    ((sha-384) (sha-384->bytevector (hmac-sha-384 secret header data)))
    ((#f) #vu8())
    (else
     (error 'cs-compute-mac
            "You forgot to put in the new MAC algorithm!" (cs-mac cs)))))

(define (cs-encrypt! cs source source-index target target-index len key IV header)
  (case (cs-cipher cs)
    ((aes-128-cbc aes-256-cbc)
     (aes-cbc-encrypt! source source-index
                       target target-index
                       len key IV))
    ((aes-128-gcm)
     ;; RFC 5246, § 6.2.3.3,  AEAD Ciphers
     ;; And RFC5288 and RFC5116.
     (aes-gcm-encrypt! source source-index
                       target target-index
                       len key IV
                       header
                       target (+ target-index len)))
    ((#f) #f)              ;null algorithm
    (else
     (error 'cs-encrypt!
            "You forgot to put in the new cipher algorithm!"
            (cs-cipher cs)))))

(define (cs-decrypt! cs source source-index target target-index len key IV header)
  (case (cs-cipher cs)
    ((aes-128-cbc aes-256-cbc)
     (aes-cbc-decrypt! source source-index
                       target target-index
                       len key IV))
    ((aes-128-gcm)
     (unless (aes-gcm-decrypt!? source source-index
                                target target-index
                                len key IV
                                header
                                source (fx+ source-index len))
       ;; The tag is invalid. Same as a bad MAC.
       (bytevector-fill! target 0)
       (error 'cs-decrypt! "Invalid tag")))
    (else
     (error 'cs-decrypt!
            "You forgot to put in the new cipher algorithm!"
            (cs-cipher cs))))))
